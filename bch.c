// ------------------------------------------------------------------------
// File:        bch.c
// Author:      Kushal Ramkumar <kramkumar@hawk.iit.edu>
// Description: An encoder/decoder for (127, 64, 10) binary BCH codes
//              Error correction using the BERLEKAMP-MASSEY ALGORITHM
// ------------------------------------------------------------------------

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

//#define DEBUG_LOGS 1

#define PRINT_POLY(x) do {\
    for(int32_t i = CODE_SIZE-1; i >=0; i--){\
        printf("%d", x[i]);\
    }\
    printf("\n");\
}while(0)

#ifdef DEBUG_LOGS
#define FUNCTION_ENTER() do{ printf("[INFO] Entering %s...\n", __func__); }while(0)
#define FUNCTION_EXIT(x) do{ printf("[INFO] Exiting %s status [%d]...\n", __func__, x); }while(0)
#else
#define FUNCTION_ENTER()
#define FUNCTION_EXIT(x)
#endif

#define LOOKUP_TABLE_SIZE 128
#define CODE_SIZE 127
#define DATA_SIZE 64
#define NUMBER_OF_ERRORS 10
#define SYNDROME_SIZE 2*NUMBER_OF_ERRORS
#define ZERO -1

/* Bit reversed g(x) = 1206534025570773100045 from Error Control Coding Lin et. all.*/
uint8_t gGeneratorPolynomial[CODE_SIZE] =
{
    1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, \
    0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, \
    1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, \
    1, 1, 0, 1, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 1, \
    0, 0\
};

/* Bit reversed p(x) = 1+x^3+x^7 from Error Control Coding Lim et.all. */
uint8_t gPrimitivePolynomial[] = { 1, 0, 0, 1, 0, 0, 0, 1 };

int8_t gAntilogLookupTable[LOOKUP_TABLE_SIZE], gLogLookupTable[LOOKUP_TABLE_SIZE];

typedef enum {
    E_STATUS_OK = 0,
    E_STATUS_MEMORY,
    E_STATUS_ERROR
}TBchErrorCodes;

typedef enum {
    BCH_ENCODE = 1,
    BCH_DECODE
}TBchOptions;

TBchErrorCodes bchPrintLookupTables()
{
    TBchErrorCodes status = E_STATUS_ERROR;
    do
    {
        if ((NULL == gLogLookupTable) || (NULL == gAntilogLookupTable))
        {
            printf("[ERROR] [%s:%d] : No memory allocated for lookup tables.", __func__, __LINE__);
            status = E_STATUS_ERROR;
            break;
        }

        printf("[INFO][%s:%d] Log Lookup Table:\n", __func__, __LINE__);
        for (int i = 0; i < LOOKUP_TABLE_SIZE; i++)
        {
            printf("[INFO] [%d] = %d\n", i, gLogLookupTable[i]);
        }

        printf("[INFO][%s:%d] Anti-Log Lookup Table:\n", __func__, __LINE__);
        for (int i = 0; i < LOOKUP_TABLE_SIZE; i++)
        {
            printf("[INFO] [%d] = %d\n", i, gAntilogLookupTable[i]);
        }
        status = E_STATUS_OK;
    } while (0);
    return status;
}

//** Find Degree of a Polynomial in GF2 **//
// Simply finds the first index of a 128bit number that is not zero
uint32_t GF2FindDegree(uint8_t *num)
{
    FUNCTION_ENTER();
    for(int32_t i=CODE_SIZE-1; i >= 0; i--)
    {
        if (1 == num[i])
        {
            return i;
        }
    }
    FUNCTION_EXIT(1);
    return 0;
}

//** Polynomial Multiplication in GF2 **//
// Executes mul = a * b
void GF2Multiply(uint8_t *a, uint8_t *b, uint8_t *mul)
{
    FUNCTION_ENTER();
    uint8_t add[CODE_SIZE] = { 0 };

    memcpy(add, b, CODE_SIZE);

    uint32_t deg = GF2FindDegree(a);
    for(uint32_t i=0; i <= deg; i++) // loop while not to the end of the poly
    {
        if(1 == a[i])       // If coeff. is a one, then add multiplicand
        {
            for(uint32_t j = 0; j < CODE_SIZE; j++)
            {
                mul[j] ^= add[j];
            }
        }
        for(uint32_t k = CODE_SIZE-1; k >= 1; k--)
        {
            add[k] = add[k-1];
        }
        add[0] = 0;
    }
    FUNCTION_EXIT(1);
}

//** Polynomial Long Division in GF2 **//
// Executes Long Division in GF2 for polynomials
// The remainder (qr[1]) is equal to the final dividend (qr[0])
// Degree of qr[1] should be smaller than degree of divisor (the break condition in the loop)
void GF2Divide(uint8_t *a, uint8_t *b, uint8_t *remainder)
{
    FUNCTION_ENTER();
    uint8_t dividend[CODE_SIZE] = { 0 }, divisor[CODE_SIZE] = { 0 },
            quotient[CODE_SIZE] = { 0 }, q[CODE_SIZE] = { 0 },
            tmp[CODE_SIZE] = { 0 };

    int32_t deg = 0;

    memcpy(dividend, a, CODE_SIZE);
    memcpy(divisor, b, CODE_SIZE);

    /* Precautionary memset */
    memset(remainder, 0, CODE_SIZE);

    while(1)    // Keep doing this until break is activated
    {
        // Subtract degrees to find what the degree of each term in the quotient
        deg = (int)(GF2FindDegree(dividend) - (int)GF2FindDegree(divisor));

        if (deg < 0)    // If negative, then you are done
        {
            memcpy(remainder, dividend, CODE_SIZE);     // return the dividend as the remainder
            return ;
        }

        if (deg > 0)    // otherwise find the appropriate degree for the term
        {
            memset(q, 0, CODE_SIZE);
            q[deg] = 1;
            q[0] ^= 1;
        }
        else
        {
            memset(q, 0, CODE_SIZE);
            q[0] = 1;
        }

        /* quotient = GF2Add(quotient, q) */
        for(int32_t i = 0; i < CODE_SIZE-1; i++)
        {
            quotient[i] ^= q[i];
        }

        /* dividend = GF2Add(dividend, GF2Multiply(quotient, divisor)) */
        memset(tmp, 0, CODE_SIZE);
        GF2Multiply(q, divisor, tmp);
        for (int32_t i = 0; i < CODE_SIZE; i++)
        {
            dividend[i] ^= tmp[i];
        }
    }
    FUNCTION_EXIT(1);
}

TBchErrorCodes bchGenerateGaloisField(uint32_t m)
{
    FUNCTION_ENTER();

    /* Initialize local variables */
    TBchErrorCodes status = E_STATUS_ERROR;
    int32_t i = 0, mask = 1, n = 0;

    do {
        /* Clear the lookup tables */
        if ((NULL == gLogLookupTable) || (NULL == gAntilogLookupTable))
        {
            printf("[ERROR] [%s:%d] : No memory allocated for lookup tables.", __func__, __LINE__);
            status = E_STATUS_ERROR;
            break;
        }
        memset(gAntilogLookupTable, 0, sizeof(gAntilogLookupTable));
        memset(gLogLookupTable, 0, sizeof(gLogLookupTable));

        /* Compute the size of the lookup table (n) */
        n = (uint32_t)pow(2, m) - 1;
        if (LOOKUP_TABLE_SIZE < n)
        {
            printf("[ERROR] [%s:%d] : Insufficient memory allocated for lookup tables. Allocated: %d, Expected: %d.", __func__, __LINE__, LOOKUP_TABLE_SIZE, n);
            status = E_STATUS_MEMORY;
            break;
        }

        gAntilogLookupTable[0] = -1;
        /* Create the first m entries in the lookup tables */
        for (int i = 0; i < m; i++)
        {
            gLogLookupTable[i] = mask;
            gAntilogLookupTable[gLogLookupTable[i]] = i;
            if (0 != gPrimitivePolynomial[i])
            {
                gLogLookupTable[m] ^= mask;
            }
            mask <<= 1;
        }

        /* Create m+1 to n entries in the lookup tables */
        gAntilogLookupTable[gLogLookupTable[m]]=m;
        mask >>= 1;
        for (int i = m + 1; i < n; i++)
        {
            if (mask <= gLogLookupTable[i - 1])
            {
                gLogLookupTable[i] = gLogLookupTable[m] ^ ((gLogLookupTable[i - 1] ^ mask) << 1);
            }
            else
            {
                gLogLookupTable[i] = gLogLookupTable[i - 1] << 1;
            }
            gAntilogLookupTable[gLogLookupTable[i]] = i;
        }
        status = E_STATUS_OK;
    } while (0);
    
    FUNCTION_EXIT(status);
    return status;
}

TBchErrorCodes bchEncode(uint32_t n, uint32_t k, uint8_t *xpData, uint8_t *xpCode)
{
    FUNCTION_ENTER();
    uint8_t tmp[CODE_SIZE] = { 0 }, xn_k[CODE_SIZE] = { 0 };

    xn_k[n-k] = 1;
    /* TODO: Code sizes hardcoded, must be make generic */
    memset(xpCode, 0, CODE_SIZE);
    
    GF2Multiply(xn_k, xpData, xpCode); /* d(x) * x**(n-k) */

    printf("d(x)*x**(n-k) (of degree %d):\n", GF2FindDegree(xpCode));
    PRINT_POLY(xpCode);

    printf("Generator Polynomial g(x):\n");
    PRINT_POLY(gGeneratorPolynomial);

    GF2Divide(xpCode, gGeneratorPolynomial, tmp);
    printf("Redundancy r(x):\n");
    PRINT_POLY(tmp);
    for(int8_t i = 0; i < CODE_SIZE; i++)
    {
        xpCode[i] ^= tmp[i];
    }
    printf("Code c(x):\n");
    PRINT_POLY(xpCode);
    FUNCTION_EXIT(1);

    return E_STATUS_OK;
}

TBchErrorCodes bchCalculateSyndromes(uint32_t n, uint8_t* xpRcvd, int8_t *xpSyndrome, int8_t *xpErrorDetected)
{
    FUNCTION_ENTER();
    /* Form the syndromes */
    for (int32_t i = 1; i <= SYNDROME_SIZE; i++)
    {
        xpSyndrome[i] = 0;
        for (int32_t j = 0; j < n; j++)
        {
            if (xpRcvd[j] != 0)
            {
                xpSyndrome[i] ^= gLogLookupTable[(i * j) % n];
            }
        }
        if (xpSyndrome[i] != 0)
        {
            *xpErrorDetected = 1;
        }
        xpSyndrome[i] = gAntilogLookupTable[xpSyndrome[i]];
    }
    FUNCTION_EXIT(1);
}

TBchErrorCodes bchBerlekampMasseyAlgorithm(int8_t* S, int8_t C[SYNDROME_SIZE + 2][SYNDROME_SIZE + 1], int8_t *L, int8_t *pErrorIndex)
{
    printf("Entering %s...\n", __func__);
    int8_t D[SYNDROME_SIZE + 1] = { 0 }, UP[SYNDROME_SIZE + 1] = { 0 };
    int8_t upMax = -1, p = 0, n = 127, i = 0;
    float pVal = 0;

    /* Assign initial values */
    C[0][0] = 0;
    C[1][0] = 1;
    D[0] = 0;
    D[1] = S[1];
    L[0] = 0;
    L[1] = 0;
    UP[0] = -1;
    UP[1] = 0;

    for (int8_t j = 1; j < SYNDROME_SIZE; j++)
    {
        C[0][j] = -1;
        C[1][j] = 0;
    }

    /* Main algorithm */
    for (i = 1; (i < SYNDROME_SIZE) && (L[i + 1] <= NUMBER_OF_ERRORS); i++)
    {
        if (D[i] == -1)
        {
            for (int8_t j = 0; j <= L[i]; j++)
            {
                C[i + 1][j] = C[i][j];
                C[i][j] = gAntilogLookupTable[C[i][j]];
            }
            L[i + 1] = L[i];
        }
        else
        {
            for (p = i - 1; ((D[p] == -1) && (p > 0));)
            {
                p--;
            }
            if (p > 0)
            {
                for (int8_t j = p - 1; j > 0; j--)
                {
                    if ((D[j] != -1) && (UP[p] < UP[j]))
                    {
                        p = j;
                    }
                }
            }

            if (L[i] > L[p] + i - p)
            {
                L[i + 1] = L[i];
            }
            else
            {
                L[i + 1] = L[p] + i - p;
            }
            
            for (int8_t j = 0; j < SYNDROME_SIZE; j++)
            {
                C[i + 1][j] = 0;
            }
            for (int8_t j = 0; j <= L[p]; j++)
            {
                if (C[p][j] != -1)
                {
                    C[i + 1][j + i - p] = gLogLookupTable[(D[i] + n - D[p] + C[p][j]) % n];
                }
            }
            for (int8_t j = 0; j <= L[i]; j++)
            {
                C[i + 1][j] ^= C[i][j];
                C[i][j] = gAntilogLookupTable[C[i][j]];
            }
        }
        UP[i + 1] = i - L[i + 1];
        if (i < SYNDROME_SIZE)
        {
            if (S[i + 1] != -1)
            {
                D[i + 1] = gLogLookupTable[S[i + 1]];
            }
            else
            {
                D[i + 1] = 0;
            }
            for (int8_t j = 1; j <= L[i + 1]; j++)
            {
                if ((S[i + 1 - j] != -1) && (C[i + 1][j] != 0))
                {
                    D[i + 1] ^= gLogLookupTable[(S[i + 1 - j] + gAntilogLookupTable[C[i + 1][j]]) % n];
                }
            }
            D[i + 1] = gAntilogLookupTable[D[i + 1]];
        }
    }
    if (L[i] <= NUMBER_OF_ERRORS)
    {
        for (int8_t j = 0; j <= L[i]; j++)
        {
            C[i][j] = gAntilogLookupTable[C[i][j]];
        }
    }
    *pErrorIndex = i;
    printf("sigma(x) = ");
    for (int8_t j = 0; j <= L[i]; j++)
    {
        printf("%3d ", C[i][j]);
    }
    printf("\n");
}

TBchErrorCodes bchChienSearch(int8_t *pxRcvd, int8_t C[SYNDROME_SIZE + 2][SYNDROME_SIZE + 1], int8_t *L, int8_t xErrorIndex)
{
    int8_t reg[SYNDROME_SIZE + 1] = { 0 }, root[CODE_SIZE + 1] = { 0 }, loc[CODE_SIZE + 1] = { 0 };
    int8_t q = 0, count = 0, n = 127;

    printf("Roots:\n");
    for (int8_t j = 1; j <= L[xErrorIndex]; j++)
    {
        reg[j] = C[xErrorIndex][j];
    }
    count = 0;
    for (int32_t j = 1; j <= n; j++)
    {
        q = 1;
        for (int32_t k = 1; k <= L[xErrorIndex]; k++)
        {
            if (reg[k] != -1)
            {
                reg[k] = (reg[k] + k) % n;
                q ^= gLogLookupTable[reg[k]];
            }
        }
        if (!q)
        {
            root[count] = j;
            loc[count] = n - j;
            count++;
            printf("%3d ", n - j);
        }
    }
    printf("\n");
    if (count == L[xErrorIndex])
    {
        /* no. roots = degree of elp hence <= t errors */
        /* Correcting Errors */
        for (int8_t j = 0; j < L[xErrorIndex]; j++)
        {
            pxRcvd[loc[j]] ^= 1;
        }
    }
    else
    {
        /* elp has degree >t hence cannot solve */
        printf("Incomplete decoding: errors detected\n");
    }
}

TBchErrorCodes bchDecode(uint32_t n, uint8_t* xpRcvd)
{
    int8_t s[SYNDROME_SIZE+1] = { 0 }, errorDetected = 0, c[SYNDROME_SIZE+1] = { 0 }, errorIndex = 0;
    int8_t C[SYNDROME_SIZE + 2][SYNDROME_SIZE + 1] = { 0 }, L[SYNDROME_SIZE + 1] = { 0 };

    /* Generate Galois Field 2**7 */
    bchGenerateGaloisField(7);
    bchCalculateSyndromes(n, xpRcvd, s, &errorDetected);

    if (errorDetected)
    {
        printf("Syndrome s(x):");
        for (int32_t i = 0; i < SYNDROME_SIZE; i++)
        {
            printf("%3d ", s[i]);
        }
        printf("\n");

        bchBerlekampMasseyAlgorithm(s, C, L, &errorIndex);

        bchChienSearch(xpRcvd, C, L, errorIndex);

        printf("Corrected C(x):");
        for (int8_t i = CODE_SIZE - 1; i >= 0; i--)
        {
            printf("%d", xpRcvd[i]);
        }
        printf("\n");
    }
    else
    {
        printf("No error detected!\n");
    }
}

void testGFArithmetic()
{
    uint8_t a[CODE_SIZE] = { 1, 1, 0, 0, 1 }, b[CODE_SIZE] = { 1, 1, 1, 1, 1 },
            c[CODE_SIZE] = { 0 };

    /*uint8_t d[CODE_SIZE] = { 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1 },
            e[CODE_SIZE] = { 1, 1, 0, 1, 1, 0, 0, 0, 1 }, f[CODE_SIZE] = { 0 };*/

    uint8_t d[CODE_SIZE] = { 1, 1, 0, 1, 1 },
                e[CODE_SIZE] = { 1, 1, 0, 1 }, f[CODE_SIZE] = { 0 }; /* result = x^2 + x */
    
    printf("------------------------\n");
    printf("GF Arithmetic Test\n");
    printf("------------------------\n");
    GF2Multiply(a, b, c);
    for (int8_t i = CODE_SIZE - 1; i >= 0; i--) {
        printf("%d", c[i]);
    }
    printf("\n");

    GF2Divide(d, e, f);
    for (int8_t i = CODE_SIZE - 1; i >= 0; i--) {
        printf("%d", f[i]);
    }
    printf("\n");

    //bchGenerateGaloisField(7);
    //bchPrintLookupTables();
}

int main(int argc, char** argv)
{
    //testGFArithmetic();

    /* Bit reversed d(x) */
    uint8_t data[CODE_SIZE] = { 0 }, code[CODE_SIZE+10] = { 0 };
    int i = 0, length = 127, k = 64, option = 0;
    FILE* inputFile = NULL;
    char* fileName[CODE_SIZE] = { 0 };

    printf("-------------------------------\n");
    printf("BCH (127,64,10) ENCODER/DECODER\n");
    printf("-------------------------------\n");
    printf("Choose an operation:\n1. Encoding\n2. Decoding\nOption >");
    scanf("%d", &option);
    printf("\n");

    if (BCH_ENCODE == option)
    {
        printf("------------------------\n");
        printf("BCH ENCODING\n");
        printf("------------------------\n");
        printf("Provide data file >");
        fscanf(stdin, "%s", fileName);

        inputFile = fopen(fileName, "r");
        if (NULL == inputFile)
        {
            printf("ERROR opening file \"%s\"!\n");
        }
        else
        {
            memset(data, 0, CODE_SIZE);
            memset(code, 0, CODE_SIZE);
            fseek(inputFile, 0, SEEK_END);

            /* Read bit-reversed D(x) */
            uint32_t i = 0;
            uint32_t cnt = ftell(inputFile);
            while (i < cnt)
            {
                i++;
#if defined(_WIN32) || defined(_WIN64)
                fseek(inputFile, -i, SEEK_END);
#else
                fseek(inputFile, cnt-i, SEEK_SET);
#endif
                if (EOF == fscanf(inputFile, "%1d", &data[i - 1]))
                {
                    printf("ERROR DATA_SIZE(%d) < %d!\n", i, DATA_SIZE);
                    return 1;
                }
            }
            printf("D(x):\n");
            PRINT_POLY(data);

            bchEncode(127, 64, data, code);
            printf("\n");
        }
    }
    else if(BCH_DECODE == option)
    {
        printf("------------------------\n");
        printf("BCH DECODING\n");
        printf("------------------------\n");
        printf("Provide code file >");
        fscanf(stdin, "%s", fileName);

        inputFile = fopen(fileName, "r");
        if (NULL == inputFile)
        {
            printf("ERROR opening file \"%s\"!\n");
        }
        else
        {
            memset(code, 0, CODE_SIZE);
            fseek(inputFile, 0, SEEK_END);
            
            /* Read bit-reversed C(x) */
            uint32_t i = 0;
            uint32_t cnt = ftell(inputFile);
            while (i < cnt)
            {
                i++;
#if defined(_WIN32) || defined(_WIN64)
                fseek(inputFile, -i, SEEK_END);
#else
                fseek(inputFile, cnt-i, SEEK_SET);
#endif
                if (EOF == fscanf(inputFile, "%1d", &code[i-1]))
                {
                    printf("ERROR CODE_SIZE(%d) < %d!\n", i, CODE_SIZE);
                    return 1;
                }
            }
            printf("C(x):\n");
            PRINT_POLY(code);
            bchDecode(127, code);
        }
    }
    else
    {
        printf("Invalid option!\n");
    }

    return 0;
}
